const fs = require('fs/promises');
const path = require('path');

const copy = require('copy-dir');

const DIR_SRC = path.resolve(__dirname, '../../src');
const DIR_DIST = path.resolve(__dirname, '../../dist');

const make = callback => new Promise(async resolve => {
  try {
    await callback();
  } catch (e) {}

  resolve();
});

const run = async () => {
  await make(() => fs.rmdir(DIR_DIST));
  await make(() => fs.mkdir(DIR_DIST));

  const items = await fs.readdir(DIR_SRC);

  for (let i = 0, len = items.length; i < len; i++) {
    const from = path.resolve(DIR_SRC,  items[i]);
    const to = path.resolve(DIR_DIST,  items[i]);

    await copy(from, to, {});
  }

  console.log('Done!');
};

run();
